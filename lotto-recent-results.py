import csv
import urllib2

resultsUrl = 'https://www.national-lottery.co.uk/results/lotto/draw-history/csv'
resultsUrlResponse = urllib2.urlopen(resultsUrl)
reader = csv.DictReader(resultsUrlResponse)
for i, row in enumerate(reader):
    if i == 0:
        print 'Latest Lotto result (%s): %s %s %s %s %s %s (%s)' % (row['DrawDate'], row['Ball 1'], row['Ball 2'], row['Ball 3'], row['Ball 4'], row['Ball 5'], row['Ball 6'], row['Bonus Ball'])
        break
